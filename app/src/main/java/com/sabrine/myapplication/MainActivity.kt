package com.sabrine.myapplication

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.sabrine.myapplication.adapter.ListFabAdapter
import com.sabrine.myapplication.entity.EventNotifier
import com.sabrine.myapplication.entity.Time
import com.sabrine.myapplication.ui.home.Purple1Fragment
import com.sabrine.myapplication.ui.home.Purple2Fragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode



class MainActivity : AppCompatActivity() {


    var list: ArrayList<Time> = ArrayList()
   lateinit var rvTime :RecyclerView
    var drawerLayout: DrawerLayout?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         drawerLayout = findViewById(R.id.drawer_layout)
        val fbAddTime: FloatingActionButton = findViewById(R.id.fb_addTime)
         rvTime = findViewById(R.id.rv_time)


        fbAddTime.setOnClickListener {
            var time:Time =Time()
            time.position=list.size+1
            time.time=System.currentTimeMillis()
      list.add(time)
            val eventNotifier = EventNotifier("sendTimeHome")
            eventNotifier.any=time
            EventBus.getDefault().post(eventNotifier)
            val eventNotifier1 = EventNotifier("sendTimeHome2")
            eventNotifier1.any=time
            EventBus.getDefault().post(eventNotifier1)
            val eventNotifier2 = EventNotifier("sendTimechild1")
            eventNotifier2.any=time
            EventBus.getDefault().post(eventNotifier2)
            val eventNotifier3 = EventNotifier("sendTimechild2")
            eventNotifier3.any=time
            EventBus.getDefault().post(eventNotifier3)
            val eventNotifier4 = EventNotifier("ShowDetail1")
            eventNotifier4.any=time
            EventBus.getDefault().post(eventNotifier4)
            val eventNotifier5 = EventNotifier("ShowDetail2")
            eventNotifier5.any=time
            EventBus.getDefault().post(eventNotifier5)
            rvTime?.adapter?.notifyDataSetChanged()
        }


        rvTime?.apply {
             layoutManager = LinearLayoutManager(context)
             adapter = ListFabAdapter(list)
        }
        val purple1Fragment =
            Purple1Fragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.content, purple1Fragment)
            .addToBackStack(null)
            .commit()
       val purple2Fragment =
            Purple2Fragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.content2, purple2Fragment)
            .addToBackStack(null)
            .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
    @SuppressLint("WrongConstant")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent( event:EventNotifier) {
        if (event.message.equals("sendTimeMain")) {
            var time:Time =Time()
            time= event.any as Time
            list.add(time)
            rvTime?.adapter?.notifyDataSetChanged()
            drawerLayout?.openDrawer(Gravity.START)
        }

    }
}
