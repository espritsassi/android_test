package com.sabrine.myapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sabrine.myapplication.R
import com.sabrine.myapplication.entity.Time

class ListFabAdapter(private val list: ArrayList<Time> )
    : RecyclerView.Adapter<ListFabAdapter.FabViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FabViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return FabViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: FabViewHolder, position: Int) {
        val time: Time = list[position]
        holder.bind(time)
    }

    override fun getItemCount(): Int = list.size


    class FabViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_time, parent, false)) {
        private var tvPosition: TextView? = null
        private var tvTime: TextView? = null


        init {
            tvPosition = itemView.findViewById(R.id.tv_position)
            tvTime = itemView.findViewById(R.id.tv_time)
        }

        fun bind(time: Time) {
            tvPosition?.text = time.position.toString()
            tvTime?.text = time.time.toString()
        }

    }
}
