package com.sabrine.myapplication

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.sabrine.myapplication.entity.Time

class sqlite(context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION){

    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "timeDatabase"
        private val TABLE_TIME = "timeTable"
        private val KEY_ID= "id"
        private val KEY_POSITION= "position"
        private val KEY_TIME= "time"
    }
    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TIME_TABLE = ("CREATE TABLE " + TABLE_TIME + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_POSITION + " INTEGER,"
                + KEY_TIME + " LONG" + ")")
        db?.execSQL(CREATE_TIME_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_TIME)
        onCreate(db)
    }



    fun addTime(emp: Time):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_POSITION, emp.position)
        contentValues.put(KEY_TIME,emp.time )
        val success = db.insert(TABLE_TIME, null, contentValues)
        db.close()
        return success
    }
    fun viewTime():List<Time>{
        val timeList:ArrayList<Time> = ArrayList<Time>()
        val selectQuery = "SELECT  * FROM $TABLE_TIME"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)

            if (cursor.moveToFirst()) {
                do {
                    var time: Time? =null
                    time?.position=cursor.getInt(1)
                    time?.time=cursor.getLong(2)
                    timeList.add(time!!)
                } while (cursor.moveToNext())
            }
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        return timeList
    }

}