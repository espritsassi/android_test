package com.sabrine.myapplication.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.sabrine.myapplication.R
import com.sabrine.myapplication.adapter.ListFabAdapter
import com.sabrine.myapplication.databinding.FragmentChildOrange1Binding
import com.sabrine.myapplication.databinding.FragmentChildOrange2Binding
import com.sabrine.myapplication.entity.EventNotifier
import com.sabrine.myapplication.entity.Time
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode



class ChildOrange2Fragment : Fragment() {

    lateinit var  binding : FragmentChildOrange2Binding
    var list: ArrayList<Time> = ArrayList()

    var linearLayout : LinearLayout?=null
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        binding = FragmentChildOrange2Binding.inflate(inflater, container, false)
        context ?: return binding.root



        binding.rvTime.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ListFabAdapter(list)
        }
        binding.fbAddTime.setOnClickListener {
            var time: Time = Time()
            time.position=list.size+1
            time.time=System.currentTimeMillis()
            list.add(time)

            val eventNotifier = EventNotifier("sendTimeMain")
            eventNotifier.any=time
            EventBus.getDefault().post(eventNotifier)
            val eventNotifier1 = EventNotifier("sendTimeHome2")
            eventNotifier1.any=time
            EventBus.getDefault().post(eventNotifier1)
            val eventNotifier2 = EventNotifier("sendTimeHome")
            eventNotifier2.any=time
            EventBus.getDefault().post(eventNotifier2)
            val eventNotifier3 = EventNotifier("sendTimechild1")
            eventNotifier3.any=time
            EventBus.getDefault().post(eventNotifier3)
            val eventNotifier4 = EventNotifier("ShowDetail1")
            eventNotifier4.any=time
            EventBus.getDefault().post(eventNotifier4)
            ShowDetail(time);
            binding.rvTime?.adapter?.notifyDataSetChanged()
        }



         linearLayout  =   binding.view2

        val fbAddTimeView1: FloatingActionButton = FloatingActionButton(requireContext())
        fbAddTimeView1.setImageResource(R.drawable.ic_tick)

        fbAddTimeView1.setOnClickListener(View.OnClickListener {
            var time: Time = Time()
            time.position=list.size+1
            time.time=System.currentTimeMillis()
            list.add(time)

            val eventNotifier = EventNotifier("sendTimeMain")
            eventNotifier.any=time
            EventBus.getDefault().post(eventNotifier)
            val eventNotifier1 = EventNotifier("sendTimeHome2")
            eventNotifier1.any=time
            EventBus.getDefault().post(eventNotifier1)
            val eventNotifier2 = EventNotifier("sendTimeHome")
            eventNotifier2.any=time
            EventBus.getDefault().post(eventNotifier2)
            val eventNotifier3 = EventNotifier("sendTimechild1")
            eventNotifier3.any=time
            EventBus.getDefault().post(eventNotifier3)

            val eventNotifier4 = EventNotifier("ShowDetail1")
            eventNotifier4.any=time
            EventBus.getDefault().post(eventNotifier4)
            binding.rvTime?.adapter?.notifyDataSetChanged()
            ShowDetail(time);
        })
        linearLayout?.addView(fbAddTimeView1)
        return binding.root
    }
    @SuppressLint("ResourceAsColor")
    fun ShowDetail(time :Time) {

        val ll = LinearLayout(context)
        ll.orientation = LinearLayout.HORIZONTAL
        val txtLabel = TextView(context)
        txtLabel.text = time.position.toString()
        txtLabel.textSize = 15F
        txtLabel.setTextColor(R.color.black)
        ll.addView(txtLabel)
        val txtSeparateur = TextView(context)
        txtSeparateur.text = "."
        txtSeparateur.setTextColor(R.color.black)
        txtSeparateur.textSize = 15F
        ll.addView(txtSeparateur)
        val txtContent = TextView(context)
        txtContent.text = time.time.toString()
        txtContent.setTextColor(R.color.black)
        txtContent.textSize = 15F
        txtContent.setTextSize(TypedValue.COMPLEX_UNIT_PT, 8f)
        ll.addView(txtContent)

        linearLayout?.addView(ll)

    }
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onResume() {
        super.onResume()
        binding.rvTime?.adapter?.notifyDataSetChanged()
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent( event:EventNotifier) {
        if (event.message.equals("sendTimechild2")) {
            var time:Time =Time()
            time= event.any as Time
            list.add(time)
            binding.rvTime?.adapter?.notifyDataSetChanged()
        }
        if (event.message.equals("ShowDetail2")) {
            var time:Time =Time()
            time= event.any as Time
            ShowDetail(time)
        }
    }
}
