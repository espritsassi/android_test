package com.sabrine.myapplication.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.sabrine.myapplication.R
import com.sabrine.myapplication.adapter.ListFabAdapter
import com.sabrine.myapplication.databinding.FragmentHomeBinding
import com.sabrine.myapplication.entity.EventNotifier
import com.sabrine.myapplication.entity.Time
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import kotlin.collections.ArrayList


class Purple1Fragment : Fragment() {

    lateinit var  binding : FragmentHomeBinding
    var list: ArrayList<Time> = ArrayList()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

         binding = FragmentHomeBinding.inflate(inflater, container, false)
        context ?: return binding.root



        binding.rvTime.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ListFabAdapter(list)
        }


        setHasOptionsMenu(true)


        binding.fbAddTime.setOnClickListener {
            var time: Time = Time()
            time.position=list.size+1
            time.time=System.currentTimeMillis()
            list.add(time)

            val eventNotifier = EventNotifier("sendTimeMain")
            eventNotifier.any=time
            EventBus.getDefault().post(eventNotifier)
            val eventNotifier1 = EventNotifier("sendTimeHome2")
            eventNotifier1.any=time
            EventBus.getDefault().post(eventNotifier1)
            val eventNotifier2 = EventNotifier("sendTimechild1")
            eventNotifier2.any=time
            EventBus.getDefault().post(eventNotifier2)
            val eventNotifier3 = EventNotifier("sendTimechild2")
            eventNotifier3.any=time
            EventBus.getDefault().post(eventNotifier3)
            val eventNotifier4 = EventNotifier("ShowDetail1")
            eventNotifier4.any=time
            EventBus.getDefault().post(eventNotifier4)
            val eventNotifier5 = EventNotifier("ShowDetail2")
            eventNotifier5.any=time
            EventBus.getDefault().post(eventNotifier5)
            binding.rvTime?.adapter?.notifyDataSetChanged()
        }


     val manager: FragmentManager = childFragmentManager
        val transaction: FragmentTransaction = manager.beginTransaction()
        transaction.replace(R.id.child1, ChildOrange1Fragment())
        transaction.commit()



        return binding.root
    }



    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent( event:EventNotifier) {
        if (event.message.equals("sendTimeHome")) {
            var time:Time =Time()
            time= event.any as Time
            list.add(time)
            binding.rvTime?.adapter?.notifyDataSetChanged()
        }

    }



}
